package vk.content;

import java.net.URLEncoder;
import java.util.Arrays;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import vk.ContentType;

public  class ContentInfo {
	
	private ContentType contentType;
	private long id;
	private long ownerId;
	private String title;	
	
	public static ContentInfo getInfo(JSONObject responseJson, ContentType contentType) throws Exception {
		
		String id, ownerId, title;
		title = "title";
		
		if(contentType == ContentType.AUDIO) {
			id = "aid"; ownerId = "owner_id";
		} else if (contentType == ContentType.VIDEO) {
			id = "vid"; ownerId = "owner_id";
		} else if (contentType == ContentType.WALL) {
			id = "id"; ownerId = "from_id";
		} else {
			throw new IllegalArgumentException("ContentType must be one of those: AUDIO, VIDEO, WALL");
		}
		 
		ContentInfo contentInfo = new ContentInfo();
		contentInfo.setContentType(contentType); 
		
		JSONObject body = responseJson;
		
		try {
			if(body.containsKey(id)) {
				contentInfo.setId((long) body.get(id));
	 		} else {
	 			throw new IllegalArgumentException("Argument does not contain id key");
	 		}
			
			if(body.containsKey(ownerId)) {
				contentInfo.setOwnerId((long) body.get(ownerId));
			} else {
				throw new IllegalArgumentException("Argument does not contain ownerId key");
			}
			
			// Not necessary parameter so no exception is thrown
			if(body.containsKey(title)) {
				contentInfo.setTitle((String) body.get(title));
			} else {
				contentInfo.setTitle("**No title**");
			}
		} catch ( ClassCastException e) {
			String msg = "Invalid argument: types are not equal. " +
				"This may be internal program error. Details: " +
					e.getMessage();
			throw new IllegalArgumentException(msg);
		}
		
		return contentInfo;
		
	}
	
	public static String getStringifyTargetIDPropName(ContentInfo contentInfo) {
		String modifier;
		ContentType ct = contentInfo.getContentType();
		
		if(ct == ContentType.WALL) modifier = "post";
		else modifier = ct.toString().toLowerCase();
		
		return modifier + "_id";
	}
	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		if(contentType == null) throw new NullPointerException();
		this.contentType = contentType;
	}

	private ContentInfo() {}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		if(title == null) throw new NullPointerException();
		this.title = title;
	}
	
	
}
