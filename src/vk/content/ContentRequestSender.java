package vk.content;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import vk.content.*;
import vk.ContentType;
import vk.user.UserAccessInfo;
import vk.generators.ContentOperationURLGenerator;

import java.io.*;
import java.net.*;
import java.rmi.ServerException;
import java.util.LinkedList;

/**
 * Created by Vlad on 28.07.2015.
 */
public abstract class ContentRequestSender {

    public static void deleteContent(ContentInfo co, UserAccessInfo ui, ContentType contentType)
            throws IOException, org.json.simple.parser.ParseException {

        ContentOperationURLGenerator contentOperationURLGenerator =
                new ContentOperationURLGenerator(ui);

        URL contentURL = null;
        try {
            contentURL = contentOperationURLGenerator.generateDeleteContentURL(co, contentType);
        } catch (MalformedURLException e) {
            throw new InternalException("Wrong contentURL sent (are you missing protocol? )");
        }

        HttpURLConnection httpURLConnection = (HttpURLConnection)
                contentURL.openConnection();

        if (httpURLConnection.getResponseCode() != 200) throw new IOException();

        BufferedReader reader = new
                BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "utf8"));

        JSONObject responseJSON = null;
        try {
            responseJSON = (JSONObject) new JSONParser().parse(reader);
        } catch (ParseException e ) {
            throw new ServerException("Server sent unexpected values");
        }
        if (responseJSON.get("error") != null) {
            JSONObject error = (JSONObject) responseJSON.get("error");

            String msg = (String) error.get("error_msg");
            Long errCode = (Long) error.get("error_code");

            throw new ServerException(String.format("Error " + errCode + "\nDescription: " + msg));
        }

    }
    public static LinkedList<ContentInfo> getContentIds(UserAccessInfo ui, ContentType contentType) 
    		throws IOException, MalformedURLException, org.json.simple.parser.ParseException, ServerException {
    	LinkedList<ContentInfo> result = new LinkedList<>();

        ContentOperationURLGenerator contentOperationURLGenerator =
                new ContentOperationURLGenerator(ui);

        URL contentURL = contentOperationURLGenerator.generateGetContentURL(contentType);
        HttpURLConnection httpURLConnection = (HttpURLConnection)
                contentURL.openConnection();

        if (httpURLConnection.getResponseCode() != 200) 
        	throw new IOException("Connection is not established properly (!= 200)");

        BufferedReader reader = new
                BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "utf8"));

        JSONObject responseJSON = (JSONObject) new JSONParser().parse(reader);

        if (responseJSON.get("error") != null) {
            JSONObject error = (JSONObject) responseJSON.get("error");

            String msg = (String) error.get("error_msg");
            Long errCode = (Long) error.get("error_code");

            throw new ServerException(String.format("Error #" + errCode + " " + msg + "\nDescription: " + msg));
        }

        JSONArray content = (JSONArray)  responseJSON.get("response");

        for(Object o : content) {
            if (o instanceof JSONObject) {
            	try{
            		result.add(ContentInfo.getInfo((JSONObject)o, contentType));
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
            }
        }

        reader.close();

        return result;
    }
}
