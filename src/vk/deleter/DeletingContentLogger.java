package vk.deleter;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Vlad on 18.10.2015.
 */
public class DeletingContentLogger implements DeletingContentListener {
    Logger l = Logger.getGlobal();

    @Override
    public void successfullyDeleted(String msg) {
        System.out.println(msg);
    }

    @Override
    public void unsuccessfullyDeleted(String msg) {
        System.out.println(msg);
    }

    @Override
    public void startDeleted() {
        String msg = "Start deleting";
        System.out.println(msg);
    }

    @Override
    public void endDeleted() {
        String msg = "End deleting";
        System.out.println(msg);
    }
}
