package vk.deleter;

/**
 * Created by vlad on 20.08.15.
 */
public interface DeletingContentListener {
    public void successfullyDeleted(String msg);
    public void unsuccessfullyDeleted(String msg);

    public void startDeleted();
    public void endDeleted();
}
