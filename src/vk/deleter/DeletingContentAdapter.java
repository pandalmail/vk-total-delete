package vk.deleter;

/**
 * Created by Vlad on 18.10.2015.
 */
public abstract class DeletingContentAdapter implements DeletingContentListener {
    @Override
    public void successfullyDeleted(String msg) {}

    @Override
    public void unsuccessfullyDeleted(String msg) {}

    @Override
    public void startDeleted() {}

    @Override
    public void endDeleted() {}
}
