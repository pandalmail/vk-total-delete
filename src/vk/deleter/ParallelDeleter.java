package vk.deleter;

import com.sun.istack.internal.NotNull;
import org.json.simple.parser.ParseException;
import vk.ContentType;
import vk.content.ContentInfo;
import vk.content.ContentRequestSender;
import vk.user.User;

import java.io.IOException;
import java.net.URLDecoder;
import java.rmi.ServerException;
import java.util.Iterator;
import java.util.LinkedList;

public class ParallelDeleter implements Runnable {

    private LinkedList<ContentInfo> contentIds;
    private Iterator<ContentInfo> iterator;
    private ContentType contentType;
    private User user;

    private Thread t;

    private transient boolean isInterrupted = false;
    private transient boolean isWorking = false;

    private LinkedList<DeletingContentListener> listeners = new LinkedList<>();

    private ParallelDeleter() {
    }

    private void contentDeletedEvent(String msg) {
        for(DeletingContentListener l : listeners) {
            l.successfullyDeleted(msg);
        }
    }

    private void contentDeletingErrorEvent(String msg) {
        for(DeletingContentListener l : listeners) {
            l.unsuccessfullyDeleted(msg);
        }
    }

    private void startDeletingEvent() {
        for(DeletingContentListener l : listeners) {
            l.startDeleted();
        }
    }

    private void endDeletingEvent() {
        for(DeletingContentListener l : listeners) {
            l.endDeleted();
        }
    }

    public void addDeletingContentListener(DeletingContentListener l) {
        listeners.add(l);
    }

    public void removeDeletingContentListener(DeletingContentListener l) {
        listeners.remove(l);
    }

    @Override
    public void run() {
        startDeletingEvent();

        while (iterator.hasNext()) {
            if (isInterrupted) {
                break;
            }

            ContentInfo curContentInfo = iterator.next();
            try {
                ContentRequestSender.deleteContent(curContentInfo, user.getUserAccessInfo(), contentType);
            } catch (Exception e) {
                contentDeletingErrorEvent(
                        String.format("Error deleting \"%s\".%n Details: %s",
                                curContentInfo.getTitle(), e.getMessage()));
            }
            contentDeletedEvent("Deleted " + curContentInfo.getContentType().toString().toLowerCase() + ": "
                    + curContentInfo.getTitle());

            try {
                // If app had no sleep, server would have force input captcha
                Thread.sleep(310);
            } catch (InterruptedException e) {
                isInterrupted = true;
                break;
            }

        }

        endDeletingEvent();
        isWorking = false;
    }

    public synchronized boolean isWorking() {
        return isWorking;
    }

    public synchronized void interrupt() {
        isInterrupted = true;
    }

    public synchronized void join() {
        try {
            t.join();
        } catch (InterruptedException e) { isInterrupted = true; }
    }

    public synchronized void deleteAllContent(ContentType contentType)
            throws ServerException, IllegalAccessException {

        if (isWorking()) {
            throw new IllegalAccessException("Deleter is busy: only one content should be deleted in a row");
        }

        isWorking = true;

        try {
            contentIds = ContentRequestSender.getContentIds(user.getUserAccessInfo(), contentType);
        } catch (IOException e) {
            throw new ServerException("An error occurred from VK servers. Details: \n" + e.getMessage());
        } catch (ParseException e) {
            throw new ServerException("Program got unexpected response. Details: \n" + e.getMessage());
        }

        iterator = contentIds.iterator();
        this.contentType = contentType;

        t = new Thread(this);
        t.start();
    }

    public static synchronized ParallelDeleter getInstance(@NotNull User u) throws NullPointerException {
        if(u == null) throw new NullPointerException("User cannot be null");

        ParallelDeleter parallelDeleter = new ParallelDeleter();
        parallelDeleter.user = u;

        return parallelDeleter;
    }

}
