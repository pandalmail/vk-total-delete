package vk;

import com.sun.istack.internal.Nullable;
import com.teamdev.jxbrowser.chromium.BrowserException;
import vk.generators.AuthorizeURLGenerator;
import vk.user.UserAccessInfo;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Exchanger;

public class Authorization {

    static AuthorizeURLGenerator authorizeGenerator =
            new AuthorizeURLGenerator(PlatformInfo.APP_ID, PlatformInfo.API_VER);


    public static UserAccessInfo getUserInfo(ContentType permissions)
            throws BrowserException {

        UserAccessInfo result = null;


        String res = "";
        URL connectionURL = null;

        try {
            connectionURL = authorizeGenerator.generateURLFromTemplate(permissions);
        } catch (MalformedURLException e) {
        }

        Exchanger<UserAccessInfo> exchanger =
                new Exchanger<UserAccessInfo>();

        BrowserWindow browserWindow = new BrowserWindow(connectionURL.toString(), exchanger);


        try {
            result = exchanger.exchange(null);
        } catch (InterruptedException e) { }

        return result;

    }

    /**
     * Tries to parse token from URL. If it is not found - returns null
     *
     * @param url where token is supposed to be parsed
     * @return String token if found, null otherwise
     */
    public static
    @Nullable
    UserAccessInfo getUserInfoFromURL(String url) {
        String tokenKey = "access_token=";
        String idKey = "user_id=";

        if (!(url.contains(tokenKey) || url.contains(idKey))) {
            String msg = "Url does not contain keys access_token or user_id";
            throw new IllegalArgumentException(msg);
        }

        String token = "";
        String id = "";
        // Search token
        int tokenStartPos = url.indexOf(tokenKey);


        for (int i = tokenStartPos + tokenKey.length(); i < url.length(); i++) {
            char currentChar = url.charAt(i);
            if (currentChar == '&') break;

            token += currentChar;
        }
        // End of token searching
        // ID Searching
        int idStartPos = url.indexOf(idKey);
        for (int i = idStartPos + idKey.length(); i < url.length(); i++) {
            char currentChar = url.charAt(i);
            if (currentChar == '&') break;

            id += currentChar;
        }

        return new UserAccessInfo(token, Long.parseLong(id));
    }
}
