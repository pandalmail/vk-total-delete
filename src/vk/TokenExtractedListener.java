package vk;

import vk.user.UserAccessInfo;

/**
 * Created by Vlad on 25.07.2015.
 */
public interface TokenExtractedListener {
    void tokenExtracted(UserAccessInfo ui);
}
