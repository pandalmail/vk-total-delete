package vk.generators;

import vk.user.UserAccessInfo;

import java.net.MalformedURLException;
import java.net.URL;
/**
 * Created by Apukhtin on 27.07.2015.
 */
public class UserContentMethodsGenerator{
    private final String _template = "https://api.vk.com/method/%s?%s&access_token=%s";

    protected UserAccessInfo _userAccessInfo;

    public  URL generateMethodURL(String methodName, String params)
        throws MalformedURLException {

        return new URL(String.format(_template, methodName, params, _userAccessInfo.getToken()));
    }

    public UserContentMethodsGenerator(UserAccessInfo ui){
        _userAccessInfo = ui;
    }
}
