package vk.generators;

import vk.ContentType;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

/**
 * Created by Vlad on 26.07.2015.
 */
public class AuthorizeURLGenerator {
    private final int APP_ID;

    private final double API_VER;

    static private final String TemplateURL = "https://oauth.vk.com/authorize?" +
            "client_id=%1$d&" +
            "scope=%2$d&" +
            "redirect_uri=https://oauth.vk.com/blank.html&" +
            "response_type=token&" +
            "v=%3$f&" +
            "state=SESSION_STATE&" + 
            "revoke=1";


    public AuthorizeURLGenerator(int app_id, double api_ver) {
        APP_ID = app_id;
        API_VER = api_ver;
    }


    /**
     * Evaluates permitions and substitutes the bitmask into the template
     * as well as unique application id and VK version.
     *
     * @param permissions
     * @return URL
     * @throws MalformedURLException
     */
    public URL generateURLFromTemplate(ContentType... permissions) throws MalformedURLException {
        int allPermissions = 0;

        if (permissions.length == 0) {
            allPermissions = ContentType.AUDIO.getValue() +
                    ContentType.WALL.getValue() +
                    ContentType.VIDEO.getValue();
        } else {
            for (ContentType c : permissions) {
                allPermissions += c.getValue();
            }
        }

        String request = String.format(TemplateURL, APP_ID, allPermissions, API_VER);
        String.copyValueOf(new char[]{});
        return new URL(request);
    }


}
