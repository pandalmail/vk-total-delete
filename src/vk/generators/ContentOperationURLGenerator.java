package vk.generators;

import vk.ContentType;
import vk.content.ContentInfo;
import vk.user.UserAccessInfo;

import java.net.MalformedURLException;
import java.net.URL;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by Vlad on 26.07.2015.
 */
public class ContentOperationURLGenerator extends UserContentMethodsGenerator {

    public URL generateGetContentURL(ContentType contentType) throws MalformedURLException{
        final String methodName = contentType.toString().toLowerCase() + ".get";
        String param = "owner_id=" + _userAccessInfo.getId();

        return generateMethodURL(methodName, param);
    }

    public URL generateDeleteContentURL(ContentInfo contentInfo, ContentType contentType) throws MalformedURLException{
    	String id, owner, type;
    	
    	type = contentType.toString().toLowerCase();    	
    	
        final String methodName = type + ".delete";
        
        final String contentIdParams = 
        		String.format("%s=%d", ContentInfo.getStringifyTargetIDPropName(contentInfo), 
        				contentInfo.getId());
        final String ownerIdParams = 
        		String.format("%s=%d", "owner_id", contentInfo.getOwnerId());
        
        final String videoSpecialParams = contentInfo.getContentType() == ContentType.VIDEO ? 
        		String.format("target_id=%d", _userAccessInfo.getId()) : "";
        
        
        String params = contentIdParams + '&' + ownerIdParams + '&'
        		+ videoSpecialParams; 

        return generateMethodURL(methodName, params);
    }

    public ContentOperationURLGenerator(UserAccessInfo ui) {
        super(ui);
    }


}
