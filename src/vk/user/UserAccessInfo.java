package vk.user;

/**
 * Created by Vlad on 26.07.2015.
 */
public class UserAccessInfo {

    private String _token;
    private long _id;

    public String getToken() {
        return _token;
    }

    public void setToken(String _token) {
        if(_token == null) throw new NullPointerException("Token cannot be null");
        this._token = _token;
    }

    public long getId() {
        return _id;
    }

    public void setId(long _id) {
        if(_id <= 0 ) throw new IllegalArgumentException("Id cannot be negative");
        this._id = _id;
    }

    public UserAccessInfo(String token, long _id) {
        setId(_id);
        setToken(token);
    }

    @Override
    public String toString() {
        return String.format("token: %s | id: %d", getToken(), getId());
    }
}
