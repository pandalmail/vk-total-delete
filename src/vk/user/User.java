package vk.user;

import vk.ContentType;
import vk.deleter.DeletingContentListener;
import vk.deleter.ParallelDeleter;

import java.rmi.ServerException;

/**
 * Created by Apukhtin on 28.07.2015.
 */
public class User {

	private ParallelDeleter parallelDeleter;
	private UserAccessInfo userAccessInfo;

	public User(UserAccessInfo uai) {
		this.userAccessInfo = uai;
		parallelDeleter = ParallelDeleter.getInstance(this);
	}

	public UserAccessInfo getUserAccessInfo() {
		return userAccessInfo;
	}

	public void setUserAccessInfo(UserAccessInfo userAccessInfo) {
		this.userAccessInfo = userAccessInfo;
	}

    public void deleteAllContent(ContentType contentType) throws IllegalAccessException, ServerException {
		if(parallelDeleter.isWorking()) throw new IllegalAccessException("Cannot give a task while deleter is busy");

		parallelDeleter.deleteAllContent(contentType);
	}

	public void addListener(DeletingContentListener listener) {
		parallelDeleter.addDeletingContentListener(listener);
	}

	public void deleteListener(DeletingContentListener listener) {
		parallelDeleter.removeDeletingContentListener(listener);
	}


}
