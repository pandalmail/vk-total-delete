package vk;

import com.teamdev.jxbrowser.chromium.BrowserException;
import vk.generators.AuthorizeURLGenerator;
import vk.user.UserAccessInfo;

import javax.swing.*;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.events.FrameLoadEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.*;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.concurrent.Exchanger;

/**
 * Created by Vlad on 25.07.2015.
 */
public class BrowserWindow extends JFrame {

    private String token;
    private long usrId;

    private Browser _browser;
    private ArrayList<TokenExtractedListener> listeners = new ArrayList<>();

    private Exchanger<UserAccessInfo> exchanger;

    public BrowserWindow() throws BrowserException {
        try {
            SwingUtilities.invokeAndWait(() -> {
                _browser = new Browser();
                add(new BrowserView(_browser));

                setSize(800, 600);
                setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                setVisible(true);
                setName("Browser");

                _browser.addLoadListener(new LoadAdapter() {

                    @Override
                    public void onDocumentLoadedInFrame(FrameLoadEvent event) {
                        super.onDocumentLoadedInFrame(event);

                        String url = event.getBrowser().getURL();


                        if (url.contains("access_token")) {

                            UserAccessInfo accessInfo =
                                    Authorization.getUserInfoFromURL(url);

                            userInfoFound(accessInfo);

                            try {
                                exchanger.exchange(accessInfo);
                            } catch (InterruptedException e) {

                            }

                          // _browser.dispose();
                            dispose();
                        }
                    }

                });
            });

        } catch (InterruptedException e) { }
        catch (InvocationTargetException | HeadlessException e) {
            throw new BrowserException(e.getMessage(), e);
        }
    }

    public BrowserWindow(String url, Exchanger<UserAccessInfo> exchanger) throws HeadlessException {

        this();
        _browser.loadURL(url);

        this.exchanger = exchanger;
    }

    public void addTokenExtractedEventListener(TokenExtractedListener l) {
        listeners.add(l);
    }

    public void removeTokenExtractedEventListener(TokenExtractedListener l) { listeners.remove(l); }

    //Event
    private void userInfoFound(UserAccessInfo ui) {
        for (TokenExtractedListener l : listeners) {
            l.tokenExtracted(ui);
        }
    }
}
