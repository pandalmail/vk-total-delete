package vk;

public enum ContentType {
    AUDIO(8), VIDEO(16), WALL(8192), ALL( AUDIO.getValue() + VIDEO.getValue() + WALL.getValue() );

    private final int value;

    private ContentType(int i){
        this.value = i;
    }

    public int getValue() {
        return value;
    }

}
